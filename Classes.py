#!/usr/bin/env python
# coding: utf-8

# In[2]:


import json
import tweepy
import codecs
import re
import pandas as pd


class Twitter():
    

    def __init__(self):
        self.api=None

    
    def authentication(self, path="twitter_credentials.json"):
        
        with open(path, "r") as file:
            creds = json.load(file)


        # Variables that contains the user credentials to access Twitter API
        # Setup tweepy to authenticate with Twitter credentials:
        auth = tweepy.OAuthHandler(creds['CONSUMER_KEY'], creds['CONSUMER_SECRET'])
        auth.set_access_token(creds['ACCESS_TOKEN'], creds['ACCESS_SECRET'])

        # Create the api to connect to twitter with your creadentials
        self.api = tweepy.API(auth)

    
    
    def tweet_preprocess(self,tweet):
        
        #get in a string the list of hashtags used by each tweet
        hashtag_list=[]
        for hashtag_entity in tweet.entities['hashtags']:
            hashtag_list.append('#'+hashtag_entity['text'])
        #save the hashtag list to a string
        hashtags_str = "-".join(x for x in hashtag_list)
        
        #remove line feed characters from tweet
        tweet_text = tweet.retweeted_status.full_text if tweet.full_text.startswith("RT @") else tweet.full_text
        tweet_text = tweet_text.replace("\r", "")
        tweet_text = tweet_text.replace("\n", "")
        tweet_text = tweet_text.replace(",", "")
        tweet_text = tweet_text.replace("\"", "'")
        tweet_text = re.sub(r"http\S+", "", tweet_text)
        tweet_text = re.sub(r"RT @\S+ ", "", tweet_text)

        
        return tweet_text, hashtags_str
    
    
    def get_tweets(self, terms=[''], limit=4000, gl='el', api=None):
        
        path = 'tweets_'+str(len(terms))+'.csv'
        
        for term in terms:
            # Saving the tweets in CSV with the following format
            # created_at, user.screen_name, user.location, text, entities.hashtags, retweet_count
            list_of_tweets = tweepy.Cursor(self.api.search, q=term, lang=gl,tweet_mode="extended", wait_on_rate_limit=True).items(limit)
        
            

            with codecs.open(path, 'a', 'utf8')as f:
                for tweet in list_of_tweets:
                    tweet_text, hashtags_str = self.tweet_preprocess(tweet)
                    f.write("%s, @%s, \"%s\", \"%s\", %s, %s \n" % (tweet.created_at, tweet.user.screen_name, tweet.user.location, tweet_text, hashtags_str, tweet.retweet_count))
        
        return path  
    
    
    def tweets_set(self,path):
        tweets = pd.read_csv(path, skipinitialspace = True, quotechar = '"', names=['Date','User','User Location','Tweet','Tags','RTs'])
        tweets = set(tweets['Tweet'])
        return tweets






import spacy
import pandas as pd
import bs4
import string





class Preprocess():
    
    def __init__(self, texts, stops, stopwords_path='stopwords.txt'):
        
        self.nlp = spacy.load('el_core_news_lg')
        #receives Python DataFrame as tweets
        self.texts_set = texts
        self.stopwords = self.read_stopwords(stopwords_path)
        self.add_extra_stopwords(stops)
        

        
    def clean_text(self, text):
        return bs4.BeautifulSoup(text, 'html.parser').get_text()
    
    
    def remove_punctuation(self,text):
        nopunc = [char  if char not in string.punctuation else " " for char in text]    
        nopunc = ''.join(nopunc)
        return nopunc
    
    
    def lemmatize(self, text):
        tokens = self.nlp(text)
        lemmas = [token.lemma_.strip() for token in tokens if not token.lemma_.isdigit() and len(token.lemma_) > 2]

        return lemmas
    
    
    def read_stopwords(self, stopwords_path = 'stopwords.txt'):
        with open(stopwords_path, 'r') as f:
            stopwords = f.read().split()
        return stopwords
        
    
    def add_extra_stopwords(self, stops, stopwords_path = 'stopwords.txt'):
        
        with open(stopwords_path, 'r') as f:
            self.stopwords = f.read().split()

        self.stopwords.append(' ')
        self.stopwords.append('')
        self.stopwords.append('amp')
        self.stopwords.append('&amp')


        stops1 =['κανω','οτι',
        'απο','ειναι','θελω','εχω','ξερω','λεω','βλεπω','aekpaok','thevoicegr',
        'covid_19','κορωνοιος','gntmgr','gdtrial', 'covid','βλεπω','κας','απο','ελα','εγω',
        'αυτο','λεω','ειπα']

        
        stops2 = ['κορονοιος',
        'κορονοιός','κορωνοιού', 'κορωνοϊού',
        'κορωνοιος','κορονοιό','κορονοιο',
        'κορωνοιός','ειναι', 'κρουσμα',
        'κορονοϊος', 'αυτο', 'νεο', 'κορονοιου',
        'κορονοϊός','κορωνοϊος','κορωνοϊός',
        'coronavirus','covid19greece','covid19gr',
        'covid','covid19','κορωνοϊό','κορονοϊό','απο','κανω','εχω']
        

        
        stops3 = ['κυβερνηση', 'primeministergr', 'μητσοτακης', 'κανω','οτι',
        'απο','ειναι','θελω','εχω','ξερω','λεω','απο']
        
        if stops == 1: stops = stops1
        elif stops == 2: stops = stops2
        elif stops == 3: stops = stops3



        for i in stops:
            self.stopwords.append(i)

    
    def clean_accent(self, word):
        word = word.replace('ά','α').replace('έ','ε').replace('ή','η')
        word = word.replace('ί','ι').replace('ό','ο').replace('ύ','υ')
        word = word.replace('ώ','ω').replace('ϊ','ι').replace('ΐ','ι')
        word = word.replace('ϋ','υ').replace('ΰ','υ')
        return word
        
        
    def remove_stopwords(self, lemmas):

        lemmas = lemmas.split(' ')

        return ' '.join([self.clean_accent(word.strip().lower()) for word in lemmas if word.lower() not in self.stopwords])


    def word_fix(self, text):

        fix_dict = {'κρουσμ':'κρουσμα', 
        'σχολει': 'σχολειο', 
        'επιχειρησ':'επιχειρηση', 
        'μασκ':'μασκα',
        'αποτελεσμ':'αποτελεσμα',
        'test':'τεστ',
        'πανδημ':'πανδημια',
        'συμπτωμ':'συμπτωμα',
        'μηνε':'μηνας',
        'μετρας':'μετρα',
        'νεας':'νεο',
        'νεος':'νεο',
        'μοιρος':'μοιρα',
        'εικονος':'εικονα',
        'αντισωμ':'αντισωμα',
        'επτο':'επτα',
        'μαθητριο':'μαθητης',
        'σημερο':'σημερα',
        'σημερας':'σημερα',
        'φαρμακας':'φαρμακο',
        'ομαδο':'ομαδα',
        'νοσοκομειας':'νοσοκομεια',
        'προβλημας':'προβλημα',
        'εμβολος':'εμβολιο',
        'υγειο':'υγεια',
        'διασωλην':'διασωληνομενος',
        'πανδημιος':'πανδημια',
        'ημερε':'ημερα',
        'μητσοτ':'μητσοτακης',
        'οπλος':'οπλο',
        'adiamantopoulou':'διαμαντοπουλου',
        'διαμαντοπ':'διαμαντοπουλου',
        'μελημο':'μελημα',
        'μελημας':'μελημα',
        'oecd':'οοσα',
        'συριζο':'συριζα',
        'συριζας':'συριζα',
        'χρονιας':'χρονια',
        'κυρωσει':'κυρωση',
        'φραχτη':'φραχτης',
        'τουρκιας':'τουρκια',
        'γηπεδα':'γηπεδο',
        'συναντησεα':'συναντηση',
        'κατσαριδ':'κατσαριδα',
        'δικστηρι':'δικαστηριο',
        'εμβολι':'εμβολιο',
        'εγκαινι':'εγκαινια',
        'κρισιμ':'κρισιμο',
        'προστασι':'προστασια',
        'οικογενει':'οικογενεια',
        'ερευνας':'ερευνα',
        'διεθνω':'διεθνης',
        'περιφερει':'περιφερεια',
        'προγραμμ':'προγραμμα'

        }

        replace_dict = {'ωρε':'ωρα',
        'χρονι':'χρονος',
        'κλινα':'κλινη',
        'εβρο':'εβρος',
        'γραμματεο':'γραμματεας',
        'ζητησα':'ζητω',
        'ασφαλεις':'ασφαλης',
        'βολτο':'βολτα',
        'χαλιας':'χαλια',
        'χαλιο':'χαλια',
        'ηλιανος':'ηλιανα',
        'παιδιο':'παιδι',
        'ανδρεο':'ανδρεας',
        'ανδρεα':'ανδρεας',
        'δικαστηριος':'δικαστηριο',
        'φορει':'φορεας',
        'στρατοπεδας':'στρατοπεδο',
        'απωλειο':'απωλεια',
        'ηπο':'ηπα',
        'ερευνο':'ερευνα',
        'ομαδας':'ομαδα',
        'παιδιας':'παιδι',
        'επιτελου':'επιτελους'
        }

        fixed_text = []
        for word in text:
            #word = self.clean_accent(word)
            for key,value in fix_dict.items():
                if key in word:
                    fixed_text.append(value)
                    fixed = True
                    break
                else:
                    fixed = False

            if not fixed:
                for key,value in replace_dict.items():
                    if key in word:
                        fixed_text.append(value)
                        replaced = True
                        break
                    else:
                        replaced = False
                if not replaced: fixed_text.append(word)
        

        return fixed_text

    
    def remove_extra_stopwords(self,text):

        clean_text = [word for word in text if word not in self.stopwords]

        return clean_text
        



    def text_preprocess(self, text):

            clean = self.clean_text(text)

            clean = self.remove_punctuation(clean)

            clean = self.remove_stopwords(clean)

            clean = self.lemmatize(clean)

            clean = self.word_fix(clean)

            clean = self.remove_extra_stopwords(clean)

            return clean

    
    def preprocess(self, texts = None):

        if texts == None: texts = self.texts_set

        clean_texts = []

        for text in texts:
            clean = self.text_preprocess(text)
            clean_texts.append(clean)

        return clean_texts
            
      

from sklearn.feature_extraction.text import CountVectorizer 
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.decomposition import LatentDirichletAllocation as LDA
import warnings
import pickle
import pandas as pd
import numpy as np
from gsdmm import MovieGroupProcess



class Topic_Modelling():


    def __init__(self):
        bow_tranformer = None



    def create_transformer(self, processing_method, texts):

        bow_tranformer = CountVectorizer(analyzer=processing_method).fit(texts)

        return bow_tranformer


    def transform_to_bow(self, transformer, texts):

        bow = transformer.transform(texts)

        return bow


    def create_tfidf_transformer(self, bow):

        tfidf_transformer = TfidfTransformer().fit(bow)

        return tfidf_transformer

    
    def transform_to_tfidf(self, tfidf_transformer, bow):

        tfidf = tfidf_transformer.transform(bow)

        return tfidf


    def fetch_tfidf(self, method, texts):

        self.bow_transformer = self.create_transformer(method,texts)
        bow = self.transform_to_bow(self.bow_transformer, texts)
        tfidf_transformer = self.create_tfidf_transformer(bow)
        tfidf = self.transform_to_tfidf(tfidf_transformer,bow)

        return tfidf


    def print_topics(self, model, count_vectorizer, n_top_words):
        
        words = self.bow_transformer.get_feature_names()

        print("Topics found via LDA:")
        for topic_idx, topic in enumerate(model.components_):
            print("\nTopic #%d:" % topic_idx)
            print(" ".join([words[i] for i in topic.argsort()[:-n_top_words - 1:-1]]))
           

    def print_lda(self, n_topics, n_words, method, texts):

        number_topics = n_topics
        number_words = n_words 
        
        lda = LDA(n_components=number_topics, n_jobs=-1)
        
        lda.fit(self.fetch_tfidf(method, texts))
        
        self.print_topics(lda, self.bow_transformer, number_words)



    def top_words(self, cluster_word_distribution, top_cluster, values):
        for cluster in top_cluster:
            sort_dicts =sorted(self.mgp.cluster_word_distribution[cluster].items(), key=lambda k: k[1], reverse=True)[:values]
            print('Topic %s : %s'%(cluster,sort_dicts))
            print(' — — — — — — — — — ')
            print()



    def gsdmm_fit(self, texts, K=5, alpha=0.1, beta=0.1, n_iters=10):
        
        self.mgp = MovieGroupProcess(K, alpha, beta, n_iters)

        vocab = set(x for doc in texts for x in doc)

        n_terms = len(vocab)
        y = self.mgp.fit(texts, n_terms)


    def print_gsdmm(self, n_words):
        
        doc_count = np.array(self.mgp.cluster_doc_count)
        
        print('Number of documents per topic :', doc_count)
        print('*'*20)
        print()

        # Topics sorted by the number of document they are allocated to

        top_index = doc_count.argsort()[-10:][::-1]
        print('Most important clusters (by number of docs inside):', top_index)
        print('*'*20)
        print()

        # Show the top 5 words in term frequency for each cluster 
        self.top_words(self.mgp.cluster_word_distribution, top_index, n_words)
